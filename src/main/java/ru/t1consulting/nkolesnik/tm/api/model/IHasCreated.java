package ru.t1consulting.nkolesnik.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasCreated {

    @Nullable
    Date getCreated();

    void setCreated(@NotNull Date created);

}
