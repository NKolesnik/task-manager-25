package ru.t1consulting.nkolesnik.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @Nullable
    Collection<AbstractCommand> getTerminalCommands();

    void add(@NotNull AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByArgument(@NotNull String argument);

}
