package ru.t1consulting.nkolesnik.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(@NotNull final String argument) {
        super("Error! Argument ``" + argument + "`` not supported...");
    }

}
