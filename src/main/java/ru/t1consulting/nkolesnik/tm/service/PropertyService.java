package ru.t1consulting.nkolesnik.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;

import java.util.Locale;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "author.name";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "author.email";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "359751";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "842685";

    @NotNull
    public static final String EMPTY_VALE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService(){
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key){
        return getStringValue(key, EMPTY_VALE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key){
        return key.replace(".","_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue){
        @NotNull String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getProperty(envKey);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }


    @Override
    public @NotNull String getApplicationVersion() {
        return getStringValue(APPLICATION_VERSION_KEY);
    }

    @Override
    public @NotNull String getAuthorName() {
        return getStringValue(AUTHOR_NAME_KEY);
    }

    @Override
    public @NotNull String getAuthorEmail() {
        return getStringValue(AUTHOR_EMAIL_KEY);
    }

    @Override
    public @NotNull String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public @NotNull Integer  getPasswordIteration() {
        @NotNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

}
